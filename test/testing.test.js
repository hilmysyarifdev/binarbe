const request = require('supertest');
const app = "http://localhost:3000";

describe('Tasks Module', () => {
    describe('GET /', () => {
      it('should return success if using valid data', async () => {
        return request(app)
        .get('/')
        .then((res) => {
          expect(res.statusCode).toBe(200);
        });
      })
    })
})